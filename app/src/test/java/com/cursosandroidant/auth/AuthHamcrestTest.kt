package com.cursosandroidant.auth


import org.hamcrest.CoreMatchers.*
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.collection.ArrayMatching
import org.hamcrest.collection.ArrayMatching.*
import org.junit.Assert
import org.junit.Ignore
import org.junit.Test


class AuthHamcrestTest {
    @Test
    fun loginUser_correctData_returnsSuccessEvent() {
        val isAuthenticated = userAuthenticationTDD("ant@gmail.com", "1234")
        assertThat(AuthEvent.USER_EXIST, `is`(isAuthenticated))
    }

    @Test
    fun loginUser_wrongData_returnsFailEvent() {
        val isAuthenticated = userAuthenticationTDD("nt@gmail.com", "1234")
        assertThat(AuthEvent.NOT_USER_EXIST, `is`(isAuthenticated))
    }

    @Test
    fun loginUser_emptyEmail_returnsEmptyEmailEvent() {
        val isAuthenticated = userAuthenticationTDD("", "1234")
        assertThat(AuthEvent.EMPTY_EMAIL, `is`(isAuthenticated))
    }

    @Test
    fun loginUser_emptyPassword_returnsEmptyPasswordEvent() {
        val isAuthenticated = userAuthenticationTDD("ant@gmail.com", "")
        assertThat(AuthEvent.EMPTY_PASSWORD, `is`(isAuthenticated))
    }

    @Test
    fun loginUser_emptyForm_returnsEmptyFormEvent() {
        val isAuthenticated = userAuthenticationTDD("", "")
        assertThat(AuthEvent.EMPTY_FORM, `is`(isAuthenticated))
    }

    @Test
    fun loginUser_invalidEmail_returnsInvalidEmailEvent() {
        val isAuthenticated = userAuthenticationTDD("ntgmail.com", "1234")
        assertThat(AuthEvent.INVALID_EMAIL, `is`(isAuthenticated))
    }

    @Test
    fun loginUser_invalidPassword_returnsInvalidPasswordEvent() {
        val isAuthenticated = userAuthenticationTDD("ant@gmail.com", "123e4")
        assertThat(AuthEvent.INVALID_PASSWORD, `is`(isAuthenticated))
    }

    @Test
    fun loginUser_invalidUser_returnsInvalidUserEvent() {
        val isAuthenticated = userAuthenticationTDD("antgmail.com", "123e4")
        assertThat(AuthEvent.INVALID_USER, `is`(isAuthenticated))
    }

    @Test(expected = AuthException::class)
    fun loginUser_nullEmail_returnsNullEmailException() {
        val isAuthenticated = userAuthenticationTDD(null, "1234")
        assertThat(AuthEvent.NULL_EMAIL, `is`(isAuthenticated))
    }

    @Test
    fun loginUser_nullPassword_returnsNullPasswordException() {
        Assert.assertThrows(AuthException::class.java) {
            userAuthenticationTDD("ant@gmail.com", null)
        }
    }


    @Test
    fun loginUser_nullForm_returnsNullFormException() {
        try {
            userAuthenticationTDD(null, null)
        } catch (e: Exception) {
            (e as? AuthException)?.let {
                assertThat(AuthEvent.NULL_FORM, `is`(it.authEvent))
            }
        }
    }

    @Ignore("Falta definir requisito del cliente")
    @Test
    fun loginUser_OverLengthPassword_returnsErrorLengthPasswordlEvent() {
        val isAuthenticated = userAuthenticationTDD("ant@gmail.com", "12343")
        assertThat(AuthEvent.ERROR_LENGTH_PASSWORD, `is`(isAuthenticated))
    }


    @Test
    fun checkNames_diferentUsers_match() {
        assertThat("Maria", both(containsString("a")).and(containsString("i")))
    }

    @Test
    fun checkData_emailAndPassword_doestnMatch() {
        val email = "ant@gmail.com"
        val password = "1234"
        assertThat(email, not(`is`(password)))
    }

    @Test
    fun newEmail_checkExist_returnString() {
        val oldEmail = "ant@gmail.com"
        val newEmail = "ant@cursoAndroid.com"
        val emails = arrayOf(oldEmail, newEmail)
        assertThat(emails, hasItemInArray(newEmail))
    }

    @Test
    fun checkDomain_arrayEmails_returnString() {
        val newEmail = "ant@cursoAndroid.com"
        val nextEmail = "alain@cursoAndroid.com"
        val emails = arrayListOf(newEmail, nextEmail)
        assertThat(emails, everyItem(endsWith("@cursoAndroid.com")))
    }

}
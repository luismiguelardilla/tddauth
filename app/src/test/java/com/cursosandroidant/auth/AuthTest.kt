package com.cursosandroidant.auth

import org.junit.Assert.*
import org.junit.Test

class AuthTest {
    @Test
    fun login_complete_returnTrue() {
        val isAuthenticated = userAuthentication("ant@gmail.com", "1234")
        assertTrue(isAuthenticated)
    }

    @Test
    fun login_complete_returnFalse() {
        val isAuthenticated = userAuthentication("nt@gmail.com", "1234")
        assertFalse(isAuthenticated)
    }

    @Test
    fun login_empty_email_returnFalse() {
        val isAuthenticated = userAuthentication(" ", "1234")
        assertFalse(isAuthenticated)
    }

   /* @Test
    fun login_null_email_returnFalse() {
        val isAuthenticated = userAuthenticationTDD(null, "1234")
        assertFalse(isAuthenticated)
    }

    @Test
    fun password_null_email_returnFalse() {
        val isAuthenticated = userAuthenticationTDD("nt@gmail.com", null)
        assertFalse(isAuthenticated)
    }
*/

}
package com.cursosandroidant.auth

import com.cursosandroidant.auth.AuthEvent.*

/****
 * Project: Auth
 * From: com.cursosandroidant.auth
 * Created by Alain Nicolás Tello on 14/12/21 at 12:05
 * All rights reserved 2021.
 *
 * All my Udemy Courses:
 * https://www.udemy.com/user/alain-nicolas-tello/
 * Web: www.alainnicolastello.com
 ***/

fun userAuthentication(email: String, password: String): Boolean {
    if (email == "ant@gmail.com" && password == "1234") {
        return true
    }
    return false
}

fun userAuthenticationTDD(email: String?, password: String?): AuthEvent {
    if (email == null && password == null) return NULL_FORM
    email ?: throw AuthException(NULL_EMAIL)
    password ?: throw AuthException(NULL_PASSWORD)
    if (email.isEmpty() && password!!.isEmpty()) return EMPTY_FORM
    val intPassword = password?.toIntOrNull()
    if (email.isEmpty()) return EMPTY_EMAIL
    if (password.isEmpty()) return EMPTY_PASSWORD
    if (!isEmailValid(email) && intPassword == null) return INVALID_USER
    if (!isEmailValid(email)) return INVALID_EMAIL
    intPassword ?: return INVALID_PASSWORD
    if (password.length > 4) return ERROR_LENGTH_PASSWORD

    return if (email == "ant@gmail.com" && password == "1234") {
        USER_EXIST
    } else {
        NOT_USER_EXIST
    }
}

fun isEmailValid(email: String): Boolean {
    val EMAIL_REGEX = "^[A-Za-z](.*)([@]{1})(.{1,})(\\.)(.{1,})"
    return EMAIL_REGEX.toRegex().matches(email)
}